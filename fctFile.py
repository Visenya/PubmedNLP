#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Nov 22 13:57:06 2017

@author: david
"""

import re
import os

def getFileListMirna():
    dir_path = os.path.dirname(os.path.abspath(__file__))
    # print(dir_path)
    with open(dir_path+"/fichier_listemirna.txt") as file:
        mirnas = file.readlines()
        return deleteEndLine(mirnas)


def getFileListTarget():
    dir_path = os.path.dirname(os.path.abspath(__file__))
    with open(dir_path+"/fichier_keyword.txt") as file:
        targets = file.readlines()
        return deleteEndLine(targets)


def deleteEndLine(fileList):
    for i in range(len(fileList)):
        regex = re.compile(r'[\n]')
        fileList[i] = regex.sub("", fileList[i])
    return fileList
