#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Nov 22 13:57:06 2017

@author: david
"""


def setAllTargetInArticles(article, listAllTarget):
    if article.listTarget:
        for target in article.listTarget:
            listAllTarget.add(target)


def setAllMirnaInArticles(article, listAllMirna):
    if article.listMirna:
        for mirna in article.listMirna:
            listAllMirna.add(mirna)


def setDicoInteraction(article, dico):
    print(article)
    for mirna in article.getListMirna():
        for target in article.getListTarget():
            if (mirna, target) not in dico:
                dico[mirna, target] = article.getIdArticle()
            else:
                stock = dico[mirna, target]
                dico[mirna, target] = stock + ", " + article.getIdArticle()