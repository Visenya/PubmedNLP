#! /usr/bin/env python3
# -*- coding: utf-8 -*-

from Bio import Entrez
from xml.etree import ElementTree as ET
from bs4 import BeautifulSoup
from Article import Article


def test():
    print("fctApi")


def searchArticleByKeyWords(database, retmax, retmode, query):
    Entrez.email = 'ddadd49@laposte.net'
    handle = Entrez.esearch(
        db = database,
        retmax = retmax,
        retmode = retmode,
        term = query)
    record = handle.read()
    handle.close()
    return record


def testAbstractTag(tree):
    for event in tree.findall('AbstractText'):
        present = event.find('AbstractText')
        if present is None:
            continue
        else:
            return True
    return False


def testAuthorTag(tree):
    for event in tree.findall('Author'):
        present = event.find('Author')
        if present is None:
            continue
        else:
            return True
    return False


def testLabTag(tree):
    for event in tree.findall('Affiliation'):
        present = event.find('Affiliation')
        if present is None:
            continue
        else:
            return True
    return False


def getApiArtcleById(database, id):
    Entrez.email = 'chetta.aline@gmail.com'
    handle = Entrez.efetch(
        db = database,
        retmode = 'xml',
        id = id)

    data = handle.read()
    # Parsing XML
    root = ET.fromstring(data)
    tree = ET.ElementTree(root)

    title = getApiTitle(tree)
    date = getApiDate(data)
    author = getApiAuthor(data)
    labo = getApiLabo(tree)
    abstract = getApiAbstract(tree)

    # idArticle = getApiId(tree)

    handle.close()

    return createArticle(id, title, date, author, labo, abstract)


def getApiAbstract(tree):
    for article in tree.iter('AbstractText'):
        return article.text


def getApiId(tree):
    for id in tree.iter('PMID'):
        print(id.text)


def getApiTitle(tree):
    for title in tree.iter('ArticleTitle'):
        return title.text


def getApiAuthor(data):
    soup = BeautifulSoup(data, "lxml")

    listAuthors = []

    for a_tag in soup.findAll("author"):
        if soup.findAll("forename") or soup.findAll("lastname"):
            if a_tag.lastname and a_tag.forename:
                listAuthors.append((a_tag.lastname.text, a_tag.forename.text))
            else:
                if a_tag.lastname:
                    listAuthors.append(a_tag.lastname.text)
                else:
                    if a_tag.forename:
                        listAuthors.append(a_tag.forename.text)
                    else:
                        if a_tag.collectivename:
                            listAuthors.append(a_tag.collectivename.text)
        else:
            if a_tag.collectivename:
                listAuthors.append(a_tag.collectivename.text)

    return listAuthors


def getApiLabo(tree):
    testTag = testAbstractTag(tree)

    for labo in tree.iter('Affiliation'):
        return labo.text


def getApiDate(data):
    soup = BeautifulSoup(data, "lxml")

    date = ""
    29154956
    if not soup.find('pubdate'):
        for a_tag in soup.findAll("pubdate"):
            date = a_tag.day.text + "/" + a_tag.month.text + "/" + a_tag.year.text
    else:
        for a_tag in soup.findAll("pubmedpubdate"):
            date = a_tag.day.text + "/" + a_tag.month.text + "/" + a_tag.year.text

    return date


def createArticle(id, title, date, author, labo, abstract):
    return Article(id, title, date, author, labo, abstract)
