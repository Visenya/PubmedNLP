Projet PubMedNLP
===

##### Réalisé dans le cadre du module 'BioInformatique Appliquée 2'
##### Professeurs : CARDIO-TOUMANIANTZ Chrystelle, EVEILLARD Damien et LE SCOUARNEC Solena
##### Auteur : _AUREGAN David_, _CHETTA Aline_ et _NAULLET Guillaume_


## But de l'application :

Cette application permet de créer à partir de mot-clés et d'une liste prédéfinies de cibles biologiques un graphique permettant selon un système de graduation de redondances d’information. Ces informations sont issues des résumés des articles présents sur PubMed.

## Lancer l'application :

    # Pré-requis :
      pip3 install Biopython
      pip3 install Biopython --upgrade
      pip3 install networkx
      sudo apt install python3-tk
      sudo apt install build-essential

    # Exécuter l'application :
      cd `cheminDuProjet`
      python3 application.py

    # Aperçu du graphique
      Installer ou mettre à jour la distribution officiel des packages JRE 7 ir 8 (JAVA).
      Unzip et se déplacer dans le dossier
      Executer ./bin/gephi
