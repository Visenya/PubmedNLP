#! /usr/bin/env python3
# -*- coding: utf-8 -*-

#########################
#        Library        #
#########################

try:
    import re
    import string
    import json
    from Bio import Entrez, Medline
    from xml.etree import ElementTree as ET
    import networkx as nx
    import fctApi
    import Article
    import fctFile
    import fctDatabase


except ImportError:
    print(
        "Une erreur s'est produite lors de l'importation des bibliothèques. \nIl est possible que certains bibliothèques ne soient pas installés dans votre environemment. \nMerci de consulter le README.")
    pass


##################################################################
#
# Article vide = 29159560, Article many tag = 29154802
#
##################################################################


##########################
#
#         Main
#
##########################

#  Variable locale
database = "pubmed"
retmax = 100
retmode = "json"
query = "(mirna AND cardiac) OR ( mirna AND cardiology )"

# Recup json Api
jsonArticle = fctApi.searchArticleByKeyWords(database, retmax, retmode, query)
print(jsonArticle)
json = json.loads(jsonArticle)

idsList = json["esearchresult"]["idlist"]

fileListMirnas = fctFile.getFileListMirna()
fileListTargets = fctFile.getFileListTarget()

listAllTarget = set()
listAllMirna = set()
dicoInteraction = {}
listnode = []

cpt = 0
for id in idsList:
    cpt += 1
    print(cpt)
    article = fctApi.getApiArtcleById(database, id)

    article.setListMirna(fileListMirnas)
    article.setListTarget(fileListTargets)

    fctDatabase.setAllTargetInArticles(article, listAllTarget)
    fctDatabase.setDicoInteraction(article, dicoInteraction)


for target in listAllTarget:
    listnode.append(target)

for mi in listAllMirna:
    listnode.append(mi)

G = nx.Graph()
G.add_nodes_from(listnode)


for cle in dicoInteraction:
    cle1 = re.findall(r'(\(\'\w+.*\',)', str(cle))
    cast = re.compile(r'[\(\']')
    cle1 = cast.sub("", str(cle1))
    cast = re.compile(r'\',')
    cle1 = cast.sub("", str(cle1))
    cle2 = re.findall(r'(, \'\w+.*\')', str(cle))
    cast = re.compile(r'[, \']')
    cle2 = cast.sub("", str(cle2))
    cast = re.compile(r'\'')
    cle2 = cast.sub("", str(cle2))
    cle1 = cle1[2:-2]
    cle2 = cle2[2:-2]
    label = dicoInteraction[cle]
    weight = 1 + dicoInteraction[cle].count(",")
    G.add_edge(str(cle1), str(cle2), Label=label, Weight=weight)
    nx.write_gexf(G, "blob.gexf")


