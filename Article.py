#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Nov 22 13:44:13 2017

@author: david
"""


class Article:
    
    def __init__(self, idArticle, titre, date, auteurs, labo, resume):
        self.idArticle = idArticle
        self.titre = titre
        self.date = date
        self.auteurs = auteurs
        self.labo = labo
        self.resume = resume
        self.listMirna = set()
        self.listTarget = set()

    def getIdArticle(self):
        return self.idArticle
    
    def getTitre(self):
        return self.titre

    def getDate(self):
        return self.date

    def getAuteur(self):
        return self.auteurs

    def getLabo(self):
        return self.labo

    def getResume(self):
        return self.resume

    def __str__(self):
        return "%s\n%s\n%s\n%s\n%s\n%s\n" % (self.idArticle, self.titre, self.date, self.auteurs, self.labo, self.resume)

    def setListMirna(self, fileMirnas):
        for i in range(len(fileMirnas)):
            if self.resume:
                if fileMirnas[i] in self.resume:
                    self.listMirna.add(fileMirnas[i])

    def getListMirna(self):
        return self.listMirna

    def setListTarget(self, fileTargets):
        for i in range(len(fileTargets)):
            if self.resume:
                if fileTargets[i] in self.resume:
                    self.listTarget.add(fileTargets[i])

    def getListTarget(self):
        return self.listTarget
